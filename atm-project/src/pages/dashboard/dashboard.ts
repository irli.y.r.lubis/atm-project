import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  toggle: boolean=true;

  withdraw:any[];

  deposit:any[];

  balance:any = "260.000.000";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.withdraw = [
      {
        value: "100.000"
      },
      {
        value: "200.000"
      },
      {
        value: "300.000"
      },
      {
        value: "400.000"
      }
    ]


    this.deposit = [
      {
        nama : "Pulsa",
        harga : "20.000"
      },
      {
        nama : "Listrik",
        harga : "30.000"
      },
      {
        nama : "Air",
        harga : "50.000"
      }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  toWithdraw(){
    this.navCtrl.push("WithdrawPage");

  }

  toDeposit(){
    this.navCtrl.push("DepositPage");
  }

  toggleButton(){
    this.toggle = !this.toggle;
  }

}
