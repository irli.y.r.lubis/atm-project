import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutentikasiPage } from './autentikasi';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    AutentikasiPage,
  ],
  imports: [
    IonicPageModule.forChild(AutentikasiPage),
    FlexLayoutModule
  ],
})
export class AutentikasiPageModule {}
