import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AutentikasiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-autentikasi',
  templateUrl: 'autentikasi.html',
})
export class AutentikasiPage {
  autentikasi:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.autentikasi = "SignUp";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AutentikasiPage');
  }

  toDashboard(){
    this.navCtrl.push("DashboardPage");
  }

}
